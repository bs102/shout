package shout

import (
	"encoding/json"
	"io"
	"io/ioutil"
	"log"
	"os"
	"os/exec"

	"gitlab.com/bs102/shout/app/models"

	"github.com/minio/minio-go"
	"github.com/satori/go.uuid"
)

var (
	minioBucketName = os.Getenv("MINIO_BUCKET_NAME")
)

// App is a struct that managing a minio client and Models object.
type App struct {
	minioClient *minio.Client
	models      *models.Models
}

// NewApp creates App object.
func NewApp() *App {
	var app App

	minioClient, err := minio.New(os.Getenv("MINIO_ENDPOINT"), os.Getenv("MINIO_ACCESS_KEY_ID"), os.Getenv("MINIO_SECRET_ACCESS_KEY"), false)
	if err != nil {
		log.Fatalln(err)
	}

	// bucket
	if exists, err := minioClient.BucketExists(minioBucketName); !exists && err == nil {
		err = minioClient.MakeBucket(minioBucketName, "")
		if err != nil {
			log.Fatalln(err)
		}
	} else if err != nil {
		log.Fatalln(err)
	}

	app.minioClient = minioClient
	app.models = models.NewModels()

	return &app
}

// GetList returns a list of voices.
func (a App) GetList() []models.ListJSON {
	return a.models.GetList()
}

// GetPost returns a detail of voice.
func (a App) GetPost(id uint64) *models.Post {
	return a.models.GetPost(id)
}

// DeletePost deletes a voice.
func (a App) DeletePost(id uint64, password string) (bool, error) {
	// list
	if ok, err := a.models.DeleteList(id, password); !ok || err != nil {
		return ok, err
	}

	// post
	var p = a.models.GetPost(id)
	if err := a.minioClient.RemoveObject(minioBucketName, p.Voice); err != nil {
		return false, err
	}
	return true, nil
}

// CreatePost creates voice.
func (a App) CreatePost(file *io.Reader, fileLen int64, password string, ip string) (interface{}, error) {
	// create UUID
	uuid, err := uuid.NewV4()
	if err != nil {
		return 0, err
	}

	// upload voice
	// TODO: check valid wav file
	voiceFileID := uuid.String()
	if _, err = a.minioClient.PutObject(minioBucketName, voiceFileID, *file, fileLen, minio.PutObjectOptions{ContentType: "audio/wav"}); err != nil {
		return 0, err
	}

	// get waveform
	waveform, err := a.createWaveForm(voiceFileID)
	if err != nil {
		return 0, err
	}

	// create list and post
	list := a.models.AddList(waveform)
	post := a.models.AddPost(list.ID, voiceFileID, password, ip)
	return struct {
		ID    uint
		Voice string
	}{post.ID, voiceFileID}, nil
}

func (a App) createWaveForm(fid string) ([]int, error) {
	// TODO: call function directly using cgo or create pure Go library
	// create temporary file
	if err := a.minioClient.FGetObject(minioBucketName, fid, "/tmp/"+fid+".wav", minio.GetObjectOptions{}); err != nil {
		return nil, err
	}

	// create waveform
	if err := exec.Command("audiowaveform", "-i", "/tmp/"+fid+".wav", "-b", "8", "--pixels-per-second", "10", "-o", "/tmp/"+fid+".json").Run(); err != nil {
		return nil, err
	}

	// parse json and get data
	type Waveform struct {
		Version         int
		Channels        int
		SampleRate      int
		SamplesPerPixel int
		Bits            int
		Length          int
		Data            []int
	}
	var waveform Waveform
	file, err := ioutil.ReadFile("/tmp/" + fid + ".json")
	if err != nil {
		return nil, err
	}
	json.Unmarshal(file, &waveform)
	return waveform.Data, nil
}
