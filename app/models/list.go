package models

import (
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/bs102/shout/app/utils"

	"github.com/jinzhu/gorm"
)

// List describes post data.
type List struct {
	gorm.Model
	Waveform string
}

// ListJSON describes post data on JSON.
type ListJSON struct {
	ID       uint
	Waveform []int
}

func init() {
	var l List
	RegisterModel(l)
}

// GetList returns a list of voices.
func (m Models) GetList() []ListJSON {
	var list []List
	var listj []ListJSON
	var count int
	m.Db.Model(&list).Count(&count)
	ids := utils.SliceIntToUint(utils.GenerateRandomNumberArray(1, count, 50))
	// It may return <50 records despite 50+ records are available because there may be deleted records.
	// But I think it's better than using order by rand()...
	m.Db.Select("id, waveform").Where("id in (?)", ids).Find(&list)
	for _, e := range list {
		var a ListJSON
		str := strings.Split(e.Waveform, ",")
		ary := make([]int, len(str))
		for i := range ary {
			ary[i], _ = strconv.Atoi(str[i])
		}
		a.ID = e.ID
		a.Waveform = ary
		listj = append(listj, a)
	}
	return listj
}

// AddList adds a voice to List.
func (m Models) AddList(waveform []int) *List {
	var list = &List{Waveform: strings.Trim(strings.Join(strings.Fields(fmt.Sprint(waveform)), ","), "[]")}
	m.Db.Create(list)
	return list
}

// DeleteList deletes the voice from List.
func (m Models) DeleteList(id uint64, password string) (bool, error) {
	if err := m.Db.Where("id = ? and password = ?", id, password).Find(&List{}).Delete(&List{}).Error; err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return false, nil
		}
		return false, err
	}
	return true, nil
}
