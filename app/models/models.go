package models

import (
	"fmt"
	"log"
	"os"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"  // for production
	_ "github.com/jinzhu/gorm/dialects/sqlite" // for development
	"github.com/revel/revel"
)

var (
	models []interface{}
)

// Models is a struct that managing gorm object.
type Models struct {
	Db *gorm.DB
}

// NewModels creates Models object.
func NewModels() *Models {
	var m Models

	var dsn string
	if os.Getenv("DB_TYPE") == "mysql" {
		dsn = fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8&parseTime=true", os.Getenv("DB_USER"), os.Getenv("DB_PASSWORD"), os.Getenv("DB_HOST"), os.Getenv("DB_PORT"), os.Getenv("DB_NAME"))
	} else if os.Getenv("DB_TYPE") == "sqlite3" {
		dsn = os.Getenv("DB_FILE")
	} else {
		log.Fatalln(os.Getenv("DB_TYPE"))
	}
	db, err := gorm.Open(os.Getenv("DB_TYPE"), dsn)
	if err != nil {
		log.Fatal(err)
	}

	if revel.DevMode {
		db.LogMode(true)
	}

	m.Db = db
	m.Init()
	return &m
}

// Init performs that migrating models to database.
func (m Models) Init() {
	for _, e := range models {
		m.Db.AutoMigrate(e)
	}
}

// RegisterModel adds given db struct to models ([]interface{}).
func RegisterModel(model interface{}) {
	models = append(models, model)
}
