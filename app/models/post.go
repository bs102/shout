package models

import (
	"github.com/jinzhu/gorm"
)

// Post describes post data.
type Post struct {
	gorm.Model
	Voice    string // file object id
	Password string
	IP       string
}

func init() {
	var p Post
	RegisterModel(p)
}

// GetPost returns a detail of voice.
func (m Models) GetPost(id uint64) *Post {
	var post Post
	m.Db.Select("id, voice").Where("id = ?", id).Find(&post)
	return &post
}

// AddPost adds a detail of voice to Post.
func (m Models) AddPost(id uint, voice string, password string, ip string) *Post {
	var post = &Post{Voice: voice, Password: password, IP: ip}
	post.ID = id
	return post
}
