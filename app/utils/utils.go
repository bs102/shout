package utils

import (
	"math/rand"
	"time"
)

// GenerateRandomNumberArray returns an array with random numbers.
func GenerateRandomNumberArray(min int, max int, n int) []int {
	var ret []int
	if max-min+1 < n {
		n = max - min + 1
	}
	now := time.Now()
	rand.Seed(int64(now.Nanosecond() ^ now.Second()))
	var a = make(map[int]bool)
	for len(ret) < n {
		var t = rand.Intn(max-min+1) + min
		if !a[t] {
			a[t] = true
			ret = append(ret, t)
		}
	}
	return ret
}

// SliceIntToUint converts from []int to []uint.
func SliceIntToUint(x []int) []uint {
	var ret []uint
	for _, e := range x {
		ret = append(ret, uint(e))
	}
	return ret
}
