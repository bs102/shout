package controllers

import (
	"fmt"
	"io"
	"log"
	"strconv"

	"gitlab.com/bs102/shout/app/shout"

	"github.com/revel/revel"
)

var (
	shoutApp *shout.App
)

// Shout is a controller.
type Shout struct {
	*revel.Controller
}

func init() {
	shoutApp = shout.NewApp()
}

// Ping returns "pong" text.
func (s Shout) Ping() revel.Result {
	return s.RenderText("pong")
}

// List returns a list of voices.
func (s Shout) List() revel.Result {
	return s.RenderJSON(shoutApp.GetList())
}

// Info returns a detail of voice.
func (s Shout) Info(id string) revel.Result {
	uintID, err := strconv.ParseUint(id, 10, 64)
	if err != nil {
		log.Println(err)
		return s.RenderError(err)
	}
	return s.RenderJSON(shoutApp.GetPost(uintID))
}

// Delete deletes a voice.
func (s Shout) Delete(id string) revel.Result {
	password := s.Params.Get("password")
	if password == "" {
		return s.Forbidden("password is empty")
	}
	uintID, err := strconv.ParseUint(id, 10, 64)
	if err != nil {
		log.Println(err)
		return s.RenderError(err)
	}

	ok, err := shoutApp.DeletePost(uintID, password)
	if err != nil {
		log.Println(err)
		return s.RenderError(err)
	}
	if !ok {
		return s.Forbidden("wrong password")
	}
	return s.RenderJSON(ok)
}

// Post creates a voice.
func (s Shout) Post() revel.Result {
	file, ok := s.Params.Files["file"]
	if !ok {
		return s.RenderError(fmt.Errorf("failed to read file"))
	}
	fileContent, err := file[0].Open()
	if err != nil {
		return s.RenderError(err)
	}

	fileContentReader := io.Reader(fileContent)
	detail, err := shoutApp.CreatePost(&fileContentReader, file[0].Size, s.Params.Get("password"), s.Request.RemoteAddr)
	if err != nil {
		return s.RenderError(err)
	}
	return s.RenderJSON(detail)
}
