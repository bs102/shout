package controllers

import (
	"github.com/revel/revel"
)

// App is a controller.
type App struct {
	*revel.Controller
}

// Index returns message.
func (c App) Index() revel.Result {
	return c.RenderText("Nothing to see here")
}
